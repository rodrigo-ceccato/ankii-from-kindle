import sqlite3
import pandas as pd
import genanki
import os
import json
import unicodedata

conn = sqlite3.connect('./vocab.db')

metadata_query = "SELECT name FROM sqlite_master WHERE type='table'"
df = pd.read_sql_query(metadata_query, conn)
# Print table names
print(df.head())

# print all tables headers
for table in df['name']:
    query = f"SELECT * FROM {table}"
    print(f"Query: {query}")
    print(pd.read_sql_query(query, conn).head())
    print("\n")

query = "SELECT words.word, words.lang, lookups.usage FROM lookups JOIN words ON lookups.word_key=words.id"
df = pd.read_sql_query(query, conn)

conn.close()

print(f"Found words: {len(df)}")
print(
    f"There are {df['word'].duplicated().sum()} duplicated words!! Dropping now...")

# Remove duplicates
df = df.drop_duplicates(subset=['word'], keep='first')

print(f"Found languages: {df['lang'].unique()}")


dict_files = [f for f in os.listdir('./dicts') if f.endswith('.json')]

dicts = {}
for f in dict_files:
    with open(f"./dicts/{f}", "r") as file:
        name = f.split('.')[0]
        print(f"Loading {f} for dict {name}")
        dict_data = json.load(file)
        dicts[name] = dict_data


cache = []
missing = []


def getDefinition(word: str, lang: str) -> str:
    # helper function to find normalized key
    def findNormalized(lang_dict: dict, normalized_word: str) -> str:
        for key in lang_dict.keys():
            normalized_key = unicodedata.normalize(
                'NFKD', key).encode('ASCII', 'ignore').lower().decode()

            if normalized_key == normalized_word:
                return key

        return None

    # Get rid of accents and lower case
    word = unicodedata.normalize('NFKD', word).encode(
        'ASCII', 'ignore').lower().decode()

    if lang not in dicts:
        print(f"Dict {lang} not found")
        return "No dict found"

    if not findNormalized(dicts[lang], word):
        print(f"Word {word} not found in dict {lang}")
        missing.append(word)
        return "No definition found"

    cache.append(word)
    key = findNormalized(dicts[lang], word)
    return dicts[lang][key]

# Strip leading and trailing whitespaces
df['word'] = df['word'].str.strip()
df['lang'] = df['lang'].str.strip()
df['usage'] = df['usage'].str.strip()
df['lang'].replace('pt-PT', 'pt', inplace=True)
df['lang'].replace('en-GB', 'en', inplace=True)
df['definition'] = df.apply(
    lambda x: getDefinition(x['word'], x['lang']), axis=1)


print(f"Transformed languages: {df['lang'].unique()}")

print(df.head())

card_data = df.to_dict(orient='records')
print(card_data[0:min(len(card_data), 3)])

# Create a new Anki model with the fields that we want
model_id = genanki.Model(
    1074094667,
    'Kindle Word Model',
    fields=[
        {'name': 'Word'},
        {'name': 'Usage'},
        {'name': 'Definition'}
    ],
    templates=[
        {
            'name': 'Card 1',
            'qfmt': '{{Word}}<br>{{Usage}}',
            'afmt': '{{FrontSide}}<hr id="answer">{{Definition}}'
        }
    ]
)

vocab_deck = genanki.Deck(
    1105776234,
    'Vocabulary'
)

print(f"Found {len(card_data)} cards to add to deck")
for card in card_data:
    vocab_deck.add_note(genanki.Note(
        model=model_id,
        fields=[card['word'], card['usage'], card['definition']]
    ))

genanki.Package(vocab_deck).write_to_file('vocab.apkg')

print(f"Found {len(missing)} missing words")
for word in missing:
    print(word)

# dump cache
with open('cache.json', 'w') as f:
    # Save the dictionary as JSON to the file
    json.dump(cache, f)
