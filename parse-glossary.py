import ebooklib
from ebooklib import epub
from bs4 import BeautifulSoup
from tqdm import tqdm
import ast
import json
import argparse
from rich import print
import os.path

parser = argparse.ArgumentParser(
    description="Generates a JSON libs from a glossary")
# Add the two string arguments
parser.add_argument("input", help="The input glossary in EPUB format.")
parser.add_argument(
    "output", help="The output JSON file. Name it as pt, en, es, etc.")

# Parse the command-line arguments
args = parser.parse_args()

print("Opening book")
book = epub.read_epub(args.input)

print("Parsing chapters")
main_text = []
for item in tqdm(book.get_items(), desc='Reading book', unit=' chapters'):
    if item.get_type() == ebooklib.ITEM_DOCUMENT:
        html_doc = item.get_content()
        soup = BeautifulSoup(html_doc, 'html.parser')
        text = soup.get_text()
        main_text.append(text)

known_split = '\n'
sane_dict = {}
# search for individual words and definitions
for chapter in tqdm(main_text, desc='Parsing chapters', unit=' chapters'):
    words = chapter.split(known_split)
    for word in words:
        word = word.lstrip("\n")
        if len(word) <= 1:
            continue
        lex = word.split()[0].lower()
        meaning = ' '.join(word.split()[1:])

        if (len(meaning) < 2):
            continue

        meaning = meaning.lstrip("\n")
        for symbol in ['*', '_', '.', '(', ')', '[', ']', '{', '}', '  ']:
            meaning = meaning.replace(f" {symbol}", f"{symbol}")
        sane_dict[lex] = meaning


print(sane_dict["cis"])
# Save the dictionary to a file
with open(f"{args.output}.json", "w") as f:
    json.dump(sane_dict, f)
